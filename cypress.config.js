const { defineConfig } = require('cypress')

module.exports = defineConfig({
  port: 3031,
  blockHosts: ['sentry.example.com'],
  e2e: {
    setupNodeEvents(on, config) {
      require('cypress-terminal-report/src/installLogsPrinter')(on)
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'http://localhost:3030',
    specPattern: 'cypress/e2e/**/*.spec.js',
  },
  component: {
    setupNodeEvents(_on, _config) {},
    specPattern: 'components/**/*.spec.js',
  },
})
