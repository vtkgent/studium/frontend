describe('Logout', () => {
  it('Checks if logging out redirects to the landing page', () => {
    cy.login()
    // Visiting the homepage should not redirect
    cy.visit('/')
    cy.url().should('eq', `${Cypress.config().baseUrl}/`)
    cy.get('div:contains("My courses")')
    // Clicking logout should redirect the user
    cy.get('.avatar').click()
    cy.contains('Logout').click()
    cy.url().should('eq', `${Cypress.config().baseUrl}/landing`)
    // Going back to the homepage should redirect the user again
    cy.visit('/')
    cy.url().should('eq', `${Cypress.config().baseUrl}/landing`)
  })
})
