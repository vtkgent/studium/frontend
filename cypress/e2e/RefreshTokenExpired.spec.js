describe('RefreshTokenExpired', () => {
  it('Checks if the user automatically logs out if the refresh token expires', () => {
    // Visiting the homepage redirects the user to the landing page
    cy.visit('/')
    // Click login on the landing page
    cy.url().should('eq', `${Cypress.config().baseUrl}/landing`)
    cy.get('span:contains("Sign in")').click()
    // Should redirect to login callback and then the homepage
    cy.url().should('eq', `${Cypress.config().baseUrl}/`)
    // The dashboard should show
    cy.get('div:contains("My courses")').should('be.visible')
    // Going back to the landing page should redirect again to the homepage
    // The page possibly won't redirect due to an error (bug in v1.0.1).
    cy.visit('/landing')
    cy.url().should('eq', `${Cypress.config().baseUrl}/`)
    // The dashboard should show
    cy.get('div:contains("My courses")').should('be.visible')
    // Make the token and refresh token expire before load
    const dateString = (Date.now() - 60 * 1000).toString()
    cy.clearCookie('apollo-token')
    cy.setCookie('auth._token_expiration.cas', dateString)
    cy.setCookie('auth._refresh_token_expiration.cas', dateString)
    // Going back to the homepage should redirect the user again
    cy.visit('/')
    cy.url().should('eq', `${Cypress.config().baseUrl}/landing`)
  })
})
