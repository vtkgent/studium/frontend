describe('Search', () => {
  it('Checks if search bar does something useful', () => {
    // This is a known issue in v1.0.2. This sequence of clicks returns a blank page.
    cy.acceptCookies()
    cy.login()
    cy.visit('/')
    cy.get('a.studium-navbar-link:contains("Search")').click()
    // Wait until the page has loaded
    cy.url().should('eq', `${Cypress.config().baseUrl}/search`)
    cy.get('label:contains("Search for a course")').should('be.visible')
    // Check if there is a clear message when the user hasn't searched
    cy.get('span:contains("Search for a course")').should('be.visible')
    // Type simple query in the search bar
    cy.get('[data-cy=search-field]').type('a')
    // Click first course available
    cy.get('[data-cy=course-link]').should('be.visible').first().click()
    // Check if a course page shows and go back
    cy.url().should('include', `/description`)
    cy.get('span:contains("Description")').should('be.visible')
    cy.get('span:contains("Subscribe")').should('be.visible')
    cy.go('back')
    // Type long query in the search bar
    cy.url().should('include', `${Cypress.config().baseUrl}/search`)
    cy.get('label:contains("Search for a course")').should('be.visible')
    cy.get('[data-cy=search-field]').type('a'.repeat(4))
    cy.get('span:contains("No courses found")').should('be.visible')
  })
  // TODO Test filters
})
