// -- This will accept the cookies --
Cypress.Commands.add('acceptCookies', () => {
  // Go to landing page
  cy.visit('/landing')
  // Click accept
  cy.get('button:contains("Accept all")').click()
  // Wait for the popup to go away
  cy.get('div:contains("We use cookies")').should('not.exist')
})

// -- This will authenticate the user --
Cypress.Commands.add('login', () => {
  // Go to landing page
  cy.visit('/landing')
  // Click login on the landing page
  cy.url().should('eq', `${Cypress.config().baseUrl}/landing`)
  cy.get('span:contains("Sign in")').click()
  // Wait for the homepage to show
  cy.url().should('eq', `${Cypress.config().baseUrl}/`)
  cy.get('div:contains("My courses")').should('be.visible')
})
