module.exports = {
  /**
   * Head
   */
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/svg+xml',
        href: '/img/icon.svg',
      },
    ],
  ],

  /**
   * Title
   */
  title: 'Studium Documentation',

  /**
   * Description
   */
  description:
    'Studium is the platform for educational tips and materials that will help you process your courses. Get involved and start contributing!',

  /**
   * Theme configuration
   */
  themeConfig: {
    logo: '/img/logo.svg',

    /**
     * Navigation
     */
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Roadmap', link: '/roadmap' },
      { text: 'Gitlab Frontend', link: 'https://gitlab.com/vtkgent/studium/backend/' },
      { text: 'Gitlab Backend', link: 'https://gitlab.com/vtkgent/studium/backend/' },
    ],

    /**
     * Sidebar
     */
    sidebar: {
      '/': [
        {
          text: 'Getting Started',
          children: [{ text: 'Installation', link: '/installation' }],
        },
        {
          text: 'Frontend',
          children: [{ text: 'Getting Started', link: '/frontend/index' }],
        },
        {
          text: 'Backend',
          children: [{ text: 'Getting Started', link: '/backend/index' }],
        },
      ],
    },
  },
}
