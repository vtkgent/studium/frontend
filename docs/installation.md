# Setup & Installation

This page contains the full development setup & installation process for the frontend and backend.

## Prerequires

For development, we recommend using `docker` and `docker-compose`.

### Windows

For the best experience on Windows, it is recommended to use WSL:

- [Install WSL 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
- Proceed to the WSL part below

### Ubuntu or WSL

- Install Git
  ```bash
  $ sudo apt update && sudo apt install -y git
  ```
- [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Install Docker Compose](https://docs.docker.com/compose/install/)

### Mac OS

- [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Install Docker](https://docs.docker.com/docker-for-mac/install/)
- [Install Docker Compose](https://docs.docker.com/compose/install/)

## Backend

### Setting up the development server

1. Start by cloning the repository:
    ```bash
    $ git clone https://github.ugent.be/VTK/studium-backend
    ```
2. Start the development server:
    ```bash
    $ make
    ```
    :::tip NOTE
    The development server will start on [http://localhost:5000](http://localhost:5000)
    :::

### Setting up the IDE

It is recommended to install and use [PyCharm](https://www.jetbrains.com/pycharm/) for the best development experience.

1. Download and install [PyCharm](https://www.jetbrains.com/pycharm/)

## Frontend

### Setting up the development server

1. Start by cloning the repository:
    ```bash
    $ git clone https://github.ugent.be/VTK/studium-frontend
    ```
2. You can start the development server by running:
    ```bash
    $ make
    ```
    :::tip NOTE
    The development server will start on [http://localhost:4000](http://localhost:4000)
    :::

### Setting up the IDE

It is recommended to install and use [Visual Studio Code](https://code.visualstudio.com/) for the best development experience.

1. Download and install [Visual Studio Code](https://code.visualstudio.com/)
2. Install the [ESLint Extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) in Visual Studio Code.