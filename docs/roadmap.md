# Roadmap of Studium

## Notification system
To increase the engagement of users of the platform, and thereby increase the quality of the platform, a notification system should be included. This notification system could 
* inform users that a course to which they are subscribed contains new files or a new description, 
* or, at the end of a semester, friendly ask the user to update some course pages. 

Features that should be added to this end:
* Course cards should be extended with a notification bell. This notification bell could possess over a v-badge component which explicitly shows the amount of notifications (if there are any).
* Course page should be extended with a notification bell and possible notification messages. For consistency, these notification messages should probably be somewhat similar to success or error messages. Note however that these notification messages should not be closed as a result of a time out but rather by an explicit close button or when leaving the page (as you don't want to see the notification next time you visit the page). 
* Log out button should probably be replaced with a profile icon or something. One possibility includes an icon with the first letter of the user's name followed by the (full) name of the logged-in user. When clicking upon this, a dropdown menu is shown with the possibilities to (i) log out, (ii) see your profile/preferences, and (iii) ...
* Alternatively/additionally, a notification bell could be added in the header when clicked upon, shows an overview of your last notifications (cfr. social media)

::: tip Note 1
We could make it so that users also receive emails from notifications. This would be cool, however, I would not set this as the default to avoid spamming our users and decreasing their user experience. Nonetheless, it could be made as a preference which could be activated through the frontend. 
:::

::: tip Note 2
It's not necessary to show notifications real-time in a window at the bottom-right or bottom-left similar to idk Facebook.
:::


## Comment system
To further increase the engagement of the platform, and allow communication between users, a comment system could be implemented. This system could allow users to ask questions about a specific topic or give remarks/corrections (especially useful since files of other people can not be removed or overwritten). 

Features that should be added to this end: 
* Filecards should be extended with a comment section (e.g. through collapsible components) OR (possibly preferably) should get their own preview page. An example of this is shown in the collapsible below and is taken from studocu
  
<details>
<summary>Preview file</summary>
**Original file table**
![](https://gitlab.com/groups/vtkgent/studium/-/wikis/uploads/aeed73621ed03512d42faa48d57b43b7/image.png)
**Preview page shown when clicked on table entry**:
![](https://gitlab.com/groups/vtkgent/studium/-/wikis/uploads/523fe1d8ff585d22d9d7ae2633b9d9af/image.png)
</details>

This comment section should probably work similarly to Facebook/Reddit where you can also start a thread on a comment. 
* Comment system can be incorporated into the abovementioned notification system. More specifically, notifications can be pushed when comments are written on one of your files, or comment has been made on a thread you're part of. 
* Extension #1: Comments should be able to be upvoted and downvoted (similar to StackOverflow). Note however that we do have a rating-component for the files, and a voting-component for comments would maybe be confusing and non-consistent. Further discussion should be made on this matter.

## Reputation/Reward system
To be able to reward people who heavily contribute to the platform or as a quality label, a reputation system similar to that of StackOverflow could be introduced. Additionally, this reputation system could be used by student associations to base prices upon. Take for example VTK where students who contribute to the wiki get some liquor vouches. 

Features that should be added to this end:
* Scoring system that takes into account the number of files uploaded, its ratings, its downloads. Furthermore, some description changes can be added into the calculation (making sure that this can not be exploited to farm reputation by for example spamming the description repeatedly). 
* The places where usernames are shown can be extended with badges
* A 'My Profile' page could be created/extended with reputation information and for example information on the amount of downloads your files have had in total etc. 



