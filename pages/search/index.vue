<!--
  Copyright: (c) 2020-2021, VTK Gent vzw
  GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)
-->

<template>
  <div>
    <!-- Search -->
    <v-row align="center">
      <!-- Search Bar -->
      <v-col cols="auto" class="flex-grow-1">
        <v-text-field
          v-model="search"
          :loading="coursesQuery.loading.value"
          :error-messages="coursesQueryError ? coursesQueryError.message : null"
          class="studium-input"
          :label="$t('search.search_label')"
          color="primary"
          hide-details="auto"
          outlined
          clearable
          data-cy="search-field"
          @keyup.enter="onSearchEnter"
        >
          <template slot="append">
            <v-icon v-if="search === ''">mdi-magnify</v-icon>

            <!-- Expand filters on mobile icon -->
            <v-icon
              v-if="$vuetify.breakpoint.xsOnly"
              :color="filterVisible ? 'primary' : ''"
              @click="filterVisible = !filterVisible"
            >
              mdi-dots-vertical
            </v-icon>
          </template>
        </v-text-field>
      </v-col>
    </v-row>

    <!-- Filters -->
    <transition name="slide-y-transition" mode="out-in">
      <v-row v-if="filterVisible" justify="space-between">
        <!-- Faculty -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.faculty"
                  :error-messages="facultiesQueryError ? facultiesQueryError.message : null"
                  :items="faculties"
                  :disabled="!!filter.program"
                  class="studium-input"
                  :label="$t('search.faculty')"
                  item-text="name"
                  item-value="code"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                  data-cy="faculty-select"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> {{ $t('search.faculty_need') }} </span>
          </v-tooltip>
        </v-col>

        <!-- Program -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-autocomplete
            v-model="filter.program"
            :error-messages="programsQueryError ? programsQueryError.message : null"
            :items="programs"
            class="studium-input"
            :label="$t('search.program')"
            item-text="name"
            item-value="code"
            hide-details="auto"
            outlined
            clearable
            single-line
            dense
            autocomplete="off"
            data-cy="program-select"
          />
        </v-col>

        <!-- Year -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.year"
                  :error-messages="yearsQueryError ? yearsQueryError.message : null"
                  :items="years"
                  :disabled="!filter.program"
                  class="studium-input"
                  :label="$t('search.year')"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                  data-cy="year-select"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> {{ $t('search.program_before') }} </span>
          </v-tooltip>
        </v-col>

        <!-- Semester -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.semester"
                  :error-messages="semestersQueryError ? semestersQueryError.message : null"
                  :items="semesters"
                  :disabled="!filter.program"
                  class="studium-input"
                  :label="$t('search.semesters')"
                  item-text="name"
                  item-value="value"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                  data-cy="semester-select"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> {{ $t('search.program_semester') }} </span>
          </v-tooltip>
        </v-col>
      </v-row>
    </transition>

    <transition name="fade" mode="out-in">
      <!-- Initial message -->
      <content-placeholder v-if="!hasSearched" key="initial" class="pt-6">
        <template #first> {{ $t('search.initial') }} </template>
      </content-placeholder>

      <!-- No courses available -->
      <content-placeholder v-else-if="!courses || courses.length === 0" key="empty" class="pt-6">
        <template #first> {{ $t('search.no_courses') }} </template>
      </content-placeholder>

      <!-- Courses -->
      <course-card-list v-else key="courses" class="pt-6" :courses="courses" :page.sync="coursesPage" />
    </transition>
  </div>
</template>

<script lang="ts">
import { computed, defineComponent, reactive, ref, useMeta, watch } from '@nuxtjs/composition-api'
import { NetworkStatus } from 'apollo-client'

import {
  useAllSemestersQuery,
  useAllYearsQuery,
  useSearchCoursesQuery,
  useSearchFacultyQuery,
  useSearchProgramQuery,
} from '~/apollo/operations'
import { useErrorHandler, useRouterQuery } from '~/util/composables'

export default defineComponent({
  setup(_, { root }) {
    const isEmpty = (v) => v === null || v === ''

    // Setup searchbar value (default from route)
    const search = useRouterQuery('search', '', { isEmpty })

    // Setup filter values (default from route)
    const filter = reactive<any>({
      faculty: useRouterQuery('faculty', null, { isEmpty }),
      program: useRouterQuery('program', null, { isEmpty }),
      year: useRouterQuery('year', null, { isEmpty }),
      semester: useRouterQuery('semester', null, { isEmpty }),
    })

    // Initialize courses query
    const coursesQuery = useSearchCoursesQuery(
      { search: search.value, ...filter },
      { throttle: 500, notifyOnNetworkStatusChange: true }
    )
    const coursesQueryError = useErrorHandler(coursesQuery)
    const courses = computed(() => coursesQuery.result.value?.allCourses?.edges.flatMap((edge) => edge?.node) ?? [])

    // Current page of results
    const coursesPage = ref(useRouterQuery('page', 1, { isEmpty }))

    // Listen to the courseQuery and reset the search page to 1 on refetch
    coursesQuery.onResult(() => {
      if (coursesQuery.networkStatus.value === NetworkStatus.refetch) {
        coursesPage.value = 1
      }
    })

    // Setup filter search values
    const filterSearch = reactive<any>({ faculty: null, program: null, year: null, semester: null, course: null })

    // Should the filters be visible
    // Filters will be visible by default on desktop and hidden on mobile.
    const filterVisible = ref(false)

    // Hide filters when going mobile
    watch(
      () => root.$vuetify.breakpoint.xsOnly,
      (isMobile) => {
        // Hide the filter when switching to mobile.
        filterVisible.value = !isMobile
      },
      { immediate: true }
    )

    // Initialize faculties query
    const facultiesQuery = useSearchFacultyQuery()
    const facultiesQueryError = useErrorHandler(facultiesQuery)
    const faculties = computed(
      () => facultiesQuery.result.value?.allFaculties?.edges.flatMap((edge) => edge?.node) ?? []
    )

    // Initialize programs query
    const programsQuery = useSearchProgramQuery({ faculty: filter.faculty })
    const programsQueryError = useErrorHandler(programsQuery)
    const programs = computed(() => programsQuery.result.value?.allPrograms?.edges.flatMap((edge) => edge?.node) ?? [])

    // Initialize years query
    const yearsQuery = useAllYearsQuery({ program: filter.program })
    const yearsQueryError = useErrorHandler(yearsQuery)
    const years = computed(() => yearsQuery.result.value?.allYears ?? [])

    // Initialize semesters query
    const semestersQuery = useAllSemestersQuery()
    const semestersQueryError = useErrorHandler(semestersQuery)
    const semesters = computed(
      () =>
        semestersQuery.result.value?.allSemesters?.map((semester) => ({
          name: semester || 'Not Specified',
          value: semester,
        })) ?? []
    )

    // If the user has searched or filtered any data
    const hasSearched = computed(
      () =>
        search.value?.length !== 0 ||
        filter.faculty !== null ||
        filter.program !== null ||
        filter.year !== null ||
        filter.semester !== null
    )

    // When the user presses enter on the search field
    // refetch the courses
    function onSearchEnter() {
      // Not the debounced value for search, since the user
      // explicitly wants to query the current search value.
      coursesQuery.variables.value = { search: search.value, ...filter }
      coursesQuery.restart()
    }

    // Update the courses when:
    // - the user types something in the search bar
    // - the filters get updated
    watch(
      () => [search, filter.faculty, filter.program, filter.year, filter.semester],
      ([search, faculty, program, year, semester]) => {
        // Refetch with new variables
        coursesQuery.variables.value = { faculty, program, year, semester, search }
        coursesQuery.restart()
      }
    )

    // Update the programs when:
    // - the user selects a faculty
    watch(
      () => filter.faculty,
      (faculty) => {
        // Refetch with new variables
        programsQuery.variables.value = { faculty }
        programsQuery.restart()
      }
    )

    // Update the years when:
    // - the user selects a program
    watch(
      () => filter.program,
      (program) => {
        // Refetch with new variables
        yearsQuery.variables.value = { program }
        yearsQuery.restart()
        filter.year = null
      }
    )

    // SEO
    useMeta(() => ({
      title: 'Search',
    }))

    return {
      search,
      coursesQuery,
      coursesQueryError,
      courses,
      coursesPage,
      filter,
      filterSearch,
      filterVisible,
      facultiesQuery,
      facultiesQueryError,
      faculties,
      programsQuery,
      programsQueryError,
      programs,
      yearsQuery,
      yearsQueryError,
      years,
      semestersQuery,
      semestersQueryError,
      semesters,
      hasSearched,
      onSearchEnter,
    }
  },

  // Empty head for useMeta
  head: {},
})
</script>
