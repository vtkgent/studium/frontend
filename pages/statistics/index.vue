<!--
  Copyright: (c) 2020-2021, VTK Gent vzw
  GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)
-->

<template>
  <suspense-query :query="statisticsQuery" :delay-loading="500" transition="page">
    <!-- Loading -->
    <template #loading>
      <page-loading />
    </template>

    <!-- Error -->
    <template #error>
      <page-error :error="statisticsError" />
    </template>

    <!-- Data -->
    <template #data>
      <div>
        <!-- Main column for the one-column layout -->
        <v-col>
          <!-- Short introduction -->
          <v-row>
            <v-col cols="12">
              <v-row>
                <h1>{{ $t('statistics.title') }}</h1>
              </v-row>
              <v-row>
                <p>
                  {{ $t('statistics.description') }}
                </p>
              </v-row>
              <v-row justify="space-between">
                <!-- Faculty -->
                <v-col cols="12" sm="6" md="3" lg="3">
                  <div>
                    <v-autocomplete
                      v-model="filter.faculty"
                      :items="faculties"
                      class="studium-input"
                      :label="$t('search.faculty')"
                      item-text="name"
                      item-value="code"
                      hide-details="auto"
                      outlined
                      clearable
                      single-line
                      dense
                      autocomplete="off"
                      data-cy="faculty-select"
                    />
                  </div>
                </v-col>
                <!-- Number -->
                <v-col cols="12" sm="6" md="3" lg="3">
                  <div>
                    <v-autocomplete
                      v-model="filter.number"
                      :items="numbers"
                      class="studium-input"
                      :label="$t('search.number')"
                      hide-details="auto"
                      outlined
                      clearable
                      single-line
                      dense
                      autocomplete="off"
                      data-cy="number-select"
                    />
                  </div>
                </v-col>
                <v-spacer />
              </v-row>
              <v-row>
                <h1>{{ $t('statistics.general.title') }}</h1>
              </v-row>
              <v-row>
                <p>
                  {{ $t('statistics.general.users') }} : <b>{{ statisticsTotal['totalUsersCount'] }}</b>
                </p>
              </v-row>
              <v-row>
                <p>
                  {{ $t('statistics.general.documents') }} : <b>{{ statisticsTotal['totalDocumentsCount'] }}</b>
                </p>
              </v-row>
            </v-col>
            <v-col cols="12" md="6">
              <v-row>
                <h1>{{ $t('statistics.weekly.title') }}</h1>
              </v-row>
              <statistics-card
                v-for="(stats, ind) in statisticsWeekly"
                :key="ind"
                :statistics="stats"
                :period="$t('statistics.weekly.period')"
              />
            </v-col>
            <v-col cols="12" md="6">
              <v-row>
                <h1>{{ $t('statistics.monthly.title') }}</h1>
              </v-row>
              <statistics-card
                v-for="(stats, ind) in statisticsMonthly"
                :key="ind"
                :statistics="stats"
                :period="$t('statistics.monthly.period')"
              />
            </v-col>
          </v-row>
          <v-row>
            <h1>{{ $t('statistics.comparison.title') }}</h1>
          </v-row>
          <v-row justify="space-between">
            <!-- Year -->
            <v-col cols="12" sm="6" md="3" lg="3">
              <div>
                <v-text-field
                  v-model="year"
                  class="studium-input"
                  hide-details="auto"
                  :label="$t('statistics.comparison.year')"
                  color="primary"
                  :disabled="comparisonLoading"
                  outlined
                />
              </div>
            </v-col>
            <v-spacer />
          </v-row>
          <v-row>
            <v-btn
              class="studium-button"
              color="primary"
              :loading="comparisonLoading"
              text
              large
              :ripple="false"
              @click="submitComparison"
            >
              {{ $t('statistics.comparison.send') }}
            </v-btn>
          </v-row>
          <div v-if="comparisonStatistics" class="mt-4">
            <v-row v-for="(society, ind) in comparisonStatistics" :key="ind">
              <v-col cols="6">
                {{ society['studentSociety'] }}
              </v-col>
              <v-col cols="6">
                {{ society['documentsUploaded'] }}
              </v-col>
            </v-row>
          </div>
        </v-col>
      </div>
    </template>
  </suspense-query>
</template>

<script lang="ts">
import { computed, defineComponent, reactive, ref, useMeta, watch } from '@nuxtjs/composition-api'

import { useComparisonStatisticsQuery, useWeeklyStatisticsQuery } from '~/apollo/_generated'
import { useSearchFacultyQuery } from '~/apollo/operations'
import { useErrorHandler, useRouterQuery } from '~/util/composables'

export default defineComponent({
  // Disable the page fade transition between tabs
  transition: 'none',
  setup() {
    const isEmpty = (v) => v === null || v === ''

    // Setup filter values (default from route)
    const filter = reactive<any>({
      faculty: useRouterQuery('faculty', 'all', { isEmpty }),
      number: useRouterQuery('number', 5, { isEmpty }),
    })

    const year = ref(new Date().getFullYear())

    const comparisonLoading = ref(false)

    // Course query and data
    const statisticsQuery = useWeeklyStatisticsQuery({ number: 5, ...filter })
    const comparisonQuery = useComparisonStatisticsQuery({ year: year.value })
    const statisticsError = useErrorHandler(statisticsQuery)
    const statisticsWeekly = computed(() => statisticsQuery.result.value?.weeklyStatistics)
    const statisticsMonthly = computed(() => statisticsQuery.result.value?.monthlyStatistics)
    const statisticsTotal = computed(() => statisticsQuery.result.value?.totalStatistics)

    const comparisonStatistics = computed(() => comparisonQuery.result.value?.comparisonStatistics ?? [])

    // SEO
    useMeta(() => ({
      title: 'Studium Statistics',
    }))

    const facultiesQuery = useSearchFacultyQuery()
    const faculties = computed(
      () => facultiesQuery.result.value?.allFaculties?.edges.flatMap((edge) => edge?.node) ?? []
    )

    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    // Submit action
    function submitComparison() {
      // Make sure the form is valid - wouldnt let me pass props.code in object
      if (year) {
        comparisonLoading.value = true
        comparisonQuery.variables.value = { year: year.value }
        comparisonQuery.restart()
        comparisonLoading.value = false
      }
    }

    watch(
      () => [filter.faculty, filter.number],
      ([faculty, number]) => {
        // Refetch with new variables
        statisticsQuery.variables.value = { number, faculty }
        statisticsQuery.restart()
      }
    )

    return {
      statisticsQuery,
      statisticsError,
      statisticsWeekly,
      statisticsMonthly,
      statisticsTotal,
      filter,
      faculties,
      numbers,
      year,
      comparisonLoading,
      comparisonStatistics,
      submitComparison,
    }
  },

  // Empty head for useMeta
  head: {},
})
</script>
