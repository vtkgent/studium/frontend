export default ({ app, $auth }) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  $auth.onRedirect((to, from) => {
    return app.localePath(to)
  })
}
