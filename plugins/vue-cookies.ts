import Vue from 'vue'
import Cookies from 'vue-cookies'

export default () => {
  Vue.use(Cookies)
}
