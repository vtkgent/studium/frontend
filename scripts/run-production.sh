#!/bin/sh
set -ex
# Cleanup child processes
alias cleanup="pkill -P \$\$"
trap cleanup INT EXIT

# Set environment
export \
  NUXT_BASE_URL=https://localhost:8080 \
  NUXT_BACKEND_PUBLIC_URL=http://localhost:5000 \
  NUXT_BACKEND_PRIVATE_URL=http://localhost:5000 \
  NUXT_MOCK_CAS=0

# Start local SSL proxy
local-ssl-proxy --source=8080 --target=3000 &

# Start production server
yarn build && yarn start
