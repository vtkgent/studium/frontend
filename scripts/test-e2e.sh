#!/bin/sh
set -ex

# Speedup docker build
export \
  COMPOSE_DOCKER_CLI_BUILD=1 \
  DOCKER_BUILDKIT=1 \
  DOCKER_DRIVER=overlay2

# Shortcut
export N="test_e2e"

# Create network for testing
docker network create "${N}"
alias cleanup_network="docker network rm \${N} >/dev/null"
trap cleanup_network EXIT INT

# Setup database
docker container run --rm --network="${N}" --name="${N}_database" --hostname="database" -e="POSTGRES_DB=studium" -e="POSTGRES_PASSWORD=postgres" -d "postgres:latest"
alias cleanup_database="docker container stop \${N}_database >/dev/null; cleanup_network"
trap cleanup_database EXIT INT
# Wait for the database
sleep 1

# Setup backend
docker container run --pull="always" --network="${N}" --name="${N}_backend" --hostname="backend" -p="8030:80" -d "registry.gitlab.com/vtkgent/studium/backend:development"
alias cleanup_backend="docker container stop \${N}_backend >/dev/null; docker container rm \${N}_backend >/dev/null; cleanup_database"
trap cleanup_backend EXIT INT
# Wait for the backend
set +x; echo "Waiting for backend ..."
until docker inspect -f "{{.State.Health.Status}}" "${N}_backend" | grep "healthy"; do sleep 0.1; done
docker inspect -f "{{.State.Health.Status}}" "${N}_backend" | grep "^healthy$" || { docker container logs "${N}_backend"; exit 1; }
echo "Backend is running"; set -x

# Set environment
export \
  NUXT_BASE_URL=http://localhost:3030 \
  NUXT_BACKEND_PUBLIC_URL=http://localhost:8030 \
  NUXT_BACKEND_PRIVATE_URL=http://localhost:8030 \
  NUXT_INSECURE_COOKIE=1 \
  NUXT_MOCK_CAS=1

# Kill leftover server
alias cleanup_frontend="pkill --full 'nuxt.*port=3030'; cleanup_backend"
trap cleanup_frontend EXIT INT

# Do not exit on error during cleanup
set +e

# Perform tests
yarn test:e2e:ci
