import { InvariantError } from 'ts-invariant'

import { MeSubscriptionsDocument } from '~/apollo/_generated'

/**
 * Pass this to `update` property of the `useSubscribeCourseMutation` or `useUnsubscribeCourseMutation`.
 * Will automatically update the cache.
 */
export function subscriptionsUpdater(store, { data: updateCourseSubscription }: any) {
  // Fetch the user subscribed courses
  let data
  try {
    data = store.readQuery({ query: MeSubscriptionsDocument })
  } catch (e) {
    // The data was not yet in the cache, don't update it
    if (e instanceof InvariantError) {
      return
    }
    // Something else happened
    throw e
  }

  // Get the course to add/remove to the user subscribed courses
  const course = updateCourseSubscription?.updateCourseSubscription?.course

  // Subscribed courses
  let subscribedCourses = data?.me?.subscriptions.edges

  // Add the course to the list if subscribed is true
  // Otherwise remove the course from the list
  if (course.subscribed) {
    subscribedCourses.push({
      __typename: 'CourseTypeEdge',
      node: course,
    })
  } else {
    subscribedCourses = subscribedCourses.filter((edge) => edge?.node?.id !== course?.id)
  }

  // Write the changed data back to the data object
  data.me.subscriptions.edges = subscribedCourses

  // Update the cache
  store.writeQuery({ query: MeSubscriptionsDocument, data })
}
