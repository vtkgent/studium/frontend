export { deepMerge } from './deep-merge'
export { isDate } from './is-date'
export { isNumeric } from './is-numeric'
export { slugify } from './slugify'
